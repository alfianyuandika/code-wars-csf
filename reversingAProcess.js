function decode(r) {
  // your code
  // create obj include char
  // console.log(r);

  let obj = {
    a: 0,
    b: 1,
    c: 2,
    d: 3,
    e: 4,
    f: 5,
    g: 6,
    h: 7,
    i: 8,
    j: 9,
    k: 10,
    l: 11,
    m: 12,
    n: 13,
    o: 14,
    p: 15,
    q: 16,
    r: 17,
    s: 18,
    t: 19,
    u: 20,
    v: 21,
    w: 22,
    x: 23,
    y: 24,
    z: 25,
  };

  // console.log(obj);
  const numArr = r.split("").filter((el) => {
    // console.log(r.split(''));
    // console.log(el);
    for (let char in obj) {
      // console.log(char);
      // console.log(obj);
      if (Number(el) === obj[char]) return el;
      // console.log(obj[char]);
    }
  });

  // decode[r] : before
  // console.log("===== LINE BEFORE SPLIT & FILTERING=====");
  // console.log(r);
  // decode[r] : after (numArr)
  // console.log("===== LINE AFTER SPLIT & FILTERING=====");
  // console.log(numArr);

  // Now we encode arrNum we split and filter before
  const encodingLetter = r
    .split("")
    .slice(numArr.length)
    .map((c) => obj[c]);
  // console.log(r.split('').slice(numArr.length));
  // we call element every char on length
  // example k = 10, dst with .pop()
  // console.log(r.split('').slice(numArr.length).map(c => obj[c]));

  let encoded = [];
  // base on arrNum element
  for (let i = 0; i < encodingLetter.length; i++) {
    for (let char in obj) {
      // arrNum to Number
      // total obj = 26 plus element inside
      if ((obj[char] * Number(numArr.join(""))) % 26 === encodingLetter[i])
        encoded.push(char);
    }
  }
  // matching between
  if (encoded.length === encodingLetter.length) return encoded.join("");
  else return "Impossible to decode";
}

